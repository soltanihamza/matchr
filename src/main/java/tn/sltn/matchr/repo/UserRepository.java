package tn.sltn.matchr.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.sltn.matchr.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

}