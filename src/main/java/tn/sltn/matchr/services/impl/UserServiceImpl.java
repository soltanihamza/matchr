package tn.sltn.matchr.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.sltn.matchr.entities.User;
import tn.sltn.matchr.repo.UserRepository;
import tn.sltn.matchr.services.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public void add(User u) {
        userRepository.save(u);
    }
}
