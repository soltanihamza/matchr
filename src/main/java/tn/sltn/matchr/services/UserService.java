package tn.sltn.matchr.services;

import org.springframework.stereotype.Service;
import tn.sltn.matchr.entities.User;

@Service
public interface UserService {

    Iterable<User> getUsers();

    void add(User u);
}
