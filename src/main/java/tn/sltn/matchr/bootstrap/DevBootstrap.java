package tn.sltn.matchr.bootstrap;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import tn.sltn.matchr.entities.User;
import tn.sltn.matchr.services.UserService;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        User admin = new User("admin","soltanixhamza@gmail.com","Spg2018$");
        userService.add(admin);
    }
}
