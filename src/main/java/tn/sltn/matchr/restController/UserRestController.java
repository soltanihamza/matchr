package tn.sltn.matchr.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.sltn.matchr.entities.User;
import tn.sltn.matchr.services.UserService;

@RestController
@RequestMapping("/api/v1")
public class UserRestController {

    @Autowired
    private UserService userService;

    @PostMapping(path="/add")
    public String adduser(@RequestParam(name="firstname", required=true) String firstname, @RequestParam(name="mail", required=true) String mail, @RequestParam(name="pwd", required=true) String pwd) {
        User u = new User(firstname,mail,pwd);
        userService.add(u);
        return "Hello "+firstname;
    }


    @GetMapping("/list")
    public Iterable<User> viewall() {
        return userService.getUsers();
    }

}
